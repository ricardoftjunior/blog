class Comment < ActiveRecord::Base

#create a relationship between post and comment
#'post' is singular because it is a many-to-one relationship
  belongs_to :post

#validation of the fields
  validates_presence_of :post_id
  validates_presence_of :body

end
