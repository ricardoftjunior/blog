class Post < ActiveRecord::Base

#create a relationship between post and comment
#'comments' is plural because it is a many-to-one relationship
#the 'dependent/destroy' part assures that all comments related to a post...
#...will be deleted when the post is deleted
  has_many :comments, dependent: :destroy

#validation of the fields
  validates_presence_of :title
  validates_presence_of :body

end
